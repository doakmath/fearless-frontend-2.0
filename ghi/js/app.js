console.log("app.js just triggered")

function createCardRow(name, description, pictureUrl, starts, ends, location) {
  return `
  <div class="row">
    <div class="col">
      <div class="card">
        <img src="${pictureUrl}" class="card-img-top">
        <div class="card-body">
            <h5 class="card-title">${name}</h5>
            <p class="card-text">${description}</p>
        </div>
        <div class="card-footer">
        ${starts}-${ends}
        </div>
      </div>
    </div>
  </div>`
}

function createCardCol(name, description, pictureUrl, starts, ends, location) {
    return `
    <div class="col">
      <div class="card">
            <img src="${pictureUrl}" class="card-img-top">
            <div class="card-body">
                <h5 class="card-title">${name}</h5>
                <p class="card-text">${description}</p>
            </div>
            <div class="card-footer">
            ${starts}-${ends}
            </div>
      </div>
    </div>
    `;
  }

  window.addEventListener('DOMContentLoaded', async () => {

    const url = 'http://localhost:8000/api/conferences/';

    try {
      const response = await fetch(url);

      if (!response.ok) {
        console.log("Oh no!  Bad response.  check async and await")
      } else {
        const data = await response.json();

        for (let conference of data.conferences) {
          const detailUrl = `http://localhost:8000${conference.href}`;
          const detailResponse = await fetch(detailUrl);
          if (detailResponse.ok) {
            const details = await detailResponse.json();
            console.log(details)
            const name = details.conference.name;
            const description = details.conference.description;
            const pictureUrl = details.conference.location.picture_url;
            let startsUTC = details.conference.starts
            let endsUTC = details.conference.ends
            let starts = new Date(startsUTC).toLocaleDateString()
            let ends = new Date(endsUTC).toLocaleDateString()
            const location = details.conference.location.name
            const html = createCardCol(name, description, pictureUrl, starts, ends, location);
            console.log(html);
            // const divTag = document.querySelector('.card')
            // divTag.innerHTML = html
            const column = document.querySelector('.col');
            column.innerHTML = html;
            const row = document.querySelector('.row');
            row.innerHTML += html
          }

        }

      }
    } catch (e) {
        console.error(e)
        console.log("else statement app.js just broke")
    }

  });
