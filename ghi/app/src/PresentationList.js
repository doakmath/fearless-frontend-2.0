import { useState, useEffect } from 'react'

function PresentationList(){
    const [presentations, setPresentations] = useState([])
    const [conferences, setConferences] = useState([])


    const fetchData = async () => {
        const response = await fetch('http://localhost:8000/api/conferences/')
        if (response.ok) {
            const { conferences } = await response.json()
            setConferences(conferences)
        } else {
            console.error("problem in PresentationList")
        }
    }

    useEffect(()=>{
        fetchData()
    }, [])

    const getData = async (conferenceId) => {
        const response = await fetch(`http://localhost:8000/api/conferences/${conferenceId}/presentations/`)
        if (response.ok) {
            const { presentations } = await response.json()
            setPresentations(presentations)
        } else {
            console.error("problem in PresentationList")
        }
    }

    const handleConference = (event) => {
        const value = event.target.value
        getData(value)
    }


    return(
        <div className="my-5 container">
            <div className="row">
                <h1>Presentation List</h1>
                <select onChange={handleConference}>
                    <option>Choose a Conference</option>
                        {conferences.map(conference => {
                            return (
                                <option key={conference.href} value={conference.id}>{conference.name}</option>
                            )
                        })}
                </select>
                <table className="table table-striped m-3">
                <thead>
                    <tr>
                    <th>href</th>
                    <th>Title</th>
                    <th>Status</th>
                    </tr>
                </thead>
                <tbody>
                    {presentations.map(presentation => {
                        return (
                            <tr key={ presentation.href }>
                                <td>{ presentation.href }</td>
                                <td>{ presentation.title }</td>
                                <td> { presentation.status }</td>
                            </tr>
                        );
                    })}
                </tbody>
                </table>
            </div>
        </div>
    )
}


    export default PresentationList
