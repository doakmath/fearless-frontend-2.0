import React, { useEffect, useState } from 'react'

function ConferenceForm(props) {
    const [locations, setLocations] = useState([])
    const [name, setName] = useState('')
    const [startDate, setStartDate] = useState("")
    const [endDate, setEndDate] = useState("")
    const [description, setDescription] = useState([])
    const [maxPresentations, setMaxPresentations] = useState(0)
    const [maxAttendees, setMaxAttendees] = useState(0)
    const [locationChange, setLocationChange] = useState()

    const handleLocation = (event) => {
        const value = event.target.value
        setLocations(value)
    }

    const handleName = (event) => {
        const value = event.target.value
        setName(value)
    }

    const handleStartDate = (event) => {
        const value = event.target.value
        setStartDate(value)
    }

    const handleEndDate = (event) => {
        const value = event.target.value
        setEndDate(value)
    }

    const handleDescription = (event) => {
        const value = event.target.value
        setDescription(value)
    }

    const handleMaxPresentations = (event) => {
        const value = event.target.value
        setMaxPresentations(value)
    }

    const handleMaxAttendees = (event) => {
        const value = event.target.value
        setMaxAttendees(value)
    }

    const handleLocationChange = (event) => {
        const value = event.target.value
        setLocationChange(value)
    }



    const handleSubmit = async (event) => {
        event.preventDefault()
        const data = {}
        data.name = name
        data.starts = startDate
        data.ends = endDate
        data.description = description
        data.max_presentations = maxPresentations
        data.max_attendees = maxAttendees
        data.location = locationChange

        console.log("data", data)

        console.log(data)
        const conferenceUrl = 'http://localhost:8000/api/conferences/'
        const fetchConfig = {
            method: "POST",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };

        const response = await fetch(conferenceUrl, fetchConfig)
        if (response.ok) {
            const newConference = await response.json()
            console.log(newConference)

            setName('')
            setStartDate('')
            setEndDate('')
            setDescription('')
            setMaxPresentations(0)
            setMaxAttendees(0)
            setLocationChange('')
        }
    }






    const fetchData = async () => {
        const url = "http://localhost:8000/api/locations/"
        const response = await fetch(url);


        if (response.ok) {
          const data = await response.json()
          setLocations(data.locations)
          }
        }


    useEffect(() => {
        fetchData();
    }, []);


    return (
        <div className="row">
            <div className="offset-3 col-6">
            <div className="shadow p-4 mt-4">
                <h1>Create a new conference</h1>
                <form onSubmit={handleSubmit} id="create-conference-form">
                <div className="form-floating mb-3">
                    <input value={name} onChange={handleName} placeholder="Name" required type="text" name="name" id="name" className="form-control" />
                    <label htmlFor="name">Name</label>
                </div>
                <div className="form-floating mb-3">
                    <input value={startDate} onChange={handleStartDate} required type="date" name="starts" id="starts" className="form-control" />
                    <label htmlFor="starts">Start Date</label>
                </div>
                <div className="form-floating mb-3">
                    <input value={endDate} onChange={handleEndDate} required type="date" name="ends" id="ends" className="form-control" />
                    <label htmlFor="ends">Ends</label>
                </div>
                <div className="form-floating mb-3">
                    <textarea value={description} onChange={handleDescription} placeholder="Description" required type="text" name="description" id="description" className="form-control"></textarea>
                    <label htmlFor="description">Description</label>
                </div>
                <div className="form-floating mb-3">
                    <input value={maxPresentations} onChange={handleMaxPresentations}placeholder="Max Presentations" required type="number" name="max_presentations" id="max_presentations" className="form-control" />
                    <label htmlFor="max_presentations">Max Presentations</label>
                </div>
                <div className="form-floating mb-3">
                    <input value={maxAttendees} onChange={handleMaxAttendees} placeholder="Max Attendees" required type="number" name="max_attendees" id="max_attendees" className="form-control" />
                    <label htmlFor="max_attendees">Max Attendees</label>
                </div>
                <div className="mb-3">
                    <select value={locationChange} onChange={handleLocationChange} required id="location" name="location" className="form-select">
                    <option value="">Choose a Location</option>
                    {locations.map(location => {
                        return (
                        <option key={location.id} value={location.id}>{location.name}</option>
                        )})}
                    </select>
                </div>
                <button className="btn btn-primary">Create</button>
                </form>
            </div>
            </div>
        </div>
    )
}

export default ConferenceForm
