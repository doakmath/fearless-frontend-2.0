import Nav from './Nav'
import AttendeesList from './AttendeesList'
import LocationForm from './LocationForm'
import ConferenceForm from './ConferenceForm'
import AttendeeConferenceForm from './AttendeeConferenceForm'
import { BrowserRouter, Routes, Route } from 'react-router-dom'
import PresentationForm from './PresentationForm'
import Mainpage from './MainPage'
import LocationList from './LocationList'
import ConferenceList from './ConferenceList'
import PresentationList from './PresentationList'
import UpdateConferenceForm from './UpdateConferenceForm'

function App(props) {
  if (props.attendees === undefined) {
    return null;
  }
  return (

    <BrowserRouter>
      <Nav />
        <div className="container"></div>
          <Routes>
            <Route>
              <Route index element={< Mainpage />} />
            </Route>
            <Route path="locations">
              <Route path="list" element={<LocationList />} />
              <Route path="new" element={<LocationForm />} />
            </Route>
            <Route path="conferences">
              <Route path="new" element={<ConferenceForm />} />
              <Route path="list" element={<ConferenceList conferences={props.conferences} />} />
              <Route path="update" element={<UpdateConferenceForm />} />
              <Route path="list/update" element={<UpdateConferenceForm />} />
            </Route>
            <Route path="attendees">
              <Route path="list" element={<AttendeesList attendees={props.attendees} />} />
              <Route path="new" element={<AttendeeConferenceForm conferences={props.conferences} />} />
            </Route>
            <Route path="presentations">
              <Route path="list" element={<PresentationList />}  />
              <Route path="new" element={<PresentationForm />}  />
            </Route>



          {/* <ConferenceForm conference={props.conference}/> */}
          {/* <LocationForm locations={props.locations} /> */}
            {/* <AttendeesList attendees={props.attendees} /> */}
      </Routes>
      {/* </div> */}
    </BrowserRouter>



  );
}

export default App;
