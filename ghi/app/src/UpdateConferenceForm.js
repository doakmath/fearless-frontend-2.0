import React, { useEffect, useState } from 'react'


function UpdateConferenceForm() {

    const [conferences, setConferences] = useState([])
    const [conference, setConference] = useState([])
    const [locations, setLocations] = useState([])
    const [location, setLocation] = useState()


    const [name, setName] = useState('')
    const [startDate, setStartDate] = useState("")
    const [endDate, setEndDate] = useState("")
    const [description, setDescription] = useState([])
    const [maxPresentations, setMaxPresentations] = useState(0)
    const [maxAttendees, setMaxAttendees] = useState(0)
    const [locationChange, setLocationChange] = useState()


    const handleConference = (event) => {
        const value = event.target.value
        setConference(value)
        getConferenceData(value)
    }

    // const handleConferences = (event) => {
    //     const value = event.target.value
    //     setConferences(value)
    // }

    const handleName = (event) => {
        const value = event.target.value
        setName(value)
    }

    // const handleStartDate = (event) => {
    //     const value = event.target.value
    //     setStartDate(value)
    // }

    // const handleEndDate = (event) => {
    //     const value = event.target.value
    //     setEndDate(value)
    // }

    const handleDescription = (event) => {
        const value = event.target.value
        setDescription(value)
    }

    // const handleMaxPresentations = (event) => {
    //     const value = event.target.value
    //     setMaxPresentations(value)
    // }

    // const handleMaxAttendees = (event) => {
    //     const value = event.target.value
    //     setMaxAttendees(value)
    // }

    const handleLocationChange = (event) => {
        const value = event.target.value
        setLocationChange(value)
        setLocation(value)
    }

    const handleSubmit = async (event) => {
        event.preventDefault()
        const data = {}
        data.name = name
        // data.starts = startDate
        // data.ends = endDate
        data.description = description
        // data.max_presentations = maxPresentations
        // data.max_attendees = maxAttendees
        data.location = locationChange
        const conferenceUrl = `http://localhost:8000/api/conferences/${conference}/`
        const fetchConfig = {
            method: "PUT",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };

        const response = await fetch(conferenceUrl, fetchConfig)
        if (response.ok) {
            setName('')
            setStartDate('')
            setEndDate('')
            setDescription('')
            setMaxPresentations(0)
            setMaxAttendees(0)
            setLocationChange('')
            setLocation('')
        }
    }

    const getConferenceData = async (conferenceId) => {

        const conferenceUrl = `http://localhost:8000/api/conferences/${conferenceId}/`
        const response = await fetch(conferenceUrl)
        if (response.ok) {
            const conference = await response.json()
            console.log("conference", conference)

            setName(conference.conference.name)
            setStartDate(conference.conference.startDate)
            setEndDate(conference.conference.endDate)
            setDescription(conference.conference.description)
            setMaxPresentations(conference.conference.maxPresentations)
            setMaxAttendees(conference.conference.maxAttendees)
            setLocationChange(conference.conference.locationChange)
            setLocation(conference.conference.location.name)
        }

    }

    const getData = async () => {
        const url = "http://localhost:8000/api/conferences/"
        const response = await fetch(url);


        if (response.ok) {
          const data = await response.json()
          setConferences(data.conferences)
          }
        }

    useEffect(() => {
        getData();
    }, []);


    const fetchData = async () => {
        const url = "http://localhost:8000/api/locations/"
        const response = await fetch(url);
        if (response.ok) {
          const data = await response.json()
          setLocations(data.locations)
          }
        }


    useEffect(() => {
        fetchData();
    }, []);

    console.log("location", location)
    return (
        <div className="row">
            <div className="offset-3 col-6">
            <div className="shadow p-4 mt-4">
                <h1>Update a conference</h1>
                <div className="mb-3">
                    <select value={conference} onChange={handleConference} required id="conference" name="conference" className="form-select">
                    <option value="">Choose a Conference</option>
                    {conferences.map(conference => {
                        return (
                        <option key={conference.id} value={conference.id}>{conference.id}-{conference.name}</option>
                        )})}
                    </select>
                </div>
                <form onSubmit={handleSubmit} id="create-conference-form">
                <div className="form-floating mb-3">
                    <input value={name} onChange={handleName} placeholder="Name" required type="text" name="name" id="name" className="form-control" />
                    <label htmlFor="name">Name</label>
                </div>
                {/* <div className="form-floating mb-3">
                    <input value={startDate} onChange={handleStartDate} required type="date" name="starts" id="starts" className="form-control" />
                    <label htmlFor="starts">Start Date</label>
                </div> */}
                {/* <div className="form-floating mb-3">
                    <input value={endDate} onChange={handleEndDate} required type="date" name="ends" id="ends" className="form-control" />
                    <label htmlFor="ends">Ends</label>
                </div> */}
                <div className="form-floating mb-3">
                    <textarea value={description} onChange={handleDescription} placeholder="Description" required type="text" name="description" id="description" className="form-control"></textarea>
                    <label htmlFor="description">Description</label>
                </div>
                {/* <div className="form-floating mb-3">
                    <input value={maxPresentations} onChange={handleMaxPresentations}placeholder="Max Presentations" required type="number" name="max_presentations" id="max_presentations" className="form-control" />
                    <label htmlFor="max_presentations">Max Presentations</label>
                </div> */}
                {/* <div className="form-floating mb-3">
                    <input value={maxAttendees} onChange={handleMaxAttendees} placeholder="Max Attendees" required type="number" name="max_attendees" id="max_attendees" className="form-control" />
                    <label htmlFor="max_attendees">Max Attendees</label>
                </div> */}
                <div className="mb-3">
                    <select value={location} onChange={handleLocationChange} required id="location" name="location" className="form-select">
                    <option value=''>{location}</option>
                    {locations.map(location => {
                        return (
                        <option key={location.id} value={location.id}>{location.name}</option>
                        )})}
                    </select>
                </div>
                <button className="btn btn-primary">Update</button>
                </form>
            </div>
            </div>
        </div>
    )
}

export default UpdateConferenceForm
