
import { useState, useEffect } from 'react'


function LocationList(){
    const [locations, setLocations] = useState([])

    const getData = async () => {
        const response = await fetch('http://localhost:8000/api/locations/')
        if (response.ok) {
            const { locations } = await response.json()
            setLocations(locations)
        } else {
            console.error("problem in LocationList")
        }
    }
    useEffect(()=>{
        getData()
    }, [])

console.log(locations)
    return(
        <div className="my-5 container">
            <div className="row">
                <h1>LocationList</h1>

                <table className="table table-striped m-3">
                <thead>
                    <tr>
                    <th>Id</th>
                    <th>href</th>
                    <th>Name</th>
                    <th>Picture url</th>
                    </tr>
                </thead>
                <tbody>
                    {locations.map(location => {
                    return (
                        <tr key={ location.id }>
                            <td>{ location.id }</td>
                            <td>{ location.href }</td>
                            <td>{ location.name }</td>
                            <td><img height='25' width='25' src={location.picture_url} alt="a picture of a city" /></td>
                        </tr>
                    );
                    })}
                </tbody>
                </table>
            </div>
        </div>
    )

    }


    export default LocationList
