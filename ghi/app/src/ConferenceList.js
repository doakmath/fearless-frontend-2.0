import { Link } from 'react-router-dom'
import { useState, useEffect } from 'react'


function ConferenceList(){
    const [conferences, setConferences] = useState([])
    const [name, setName] = useState([])
    const [description, setDescription] = useState([])

    const getData = async () => {
        const response = await fetch('http://localhost:8000/api/conferences/')
        if (response.ok) {
            const { conferences } = await response.json()
            setConferences(conferences)
        } else {
            console.error("problem in conferenceList")
        }
    }
    useEffect(()=>{
        getData()
    }, [])

    async function handleDelete(id) {
        const fetchConfig = { method: "DELETE"}
        const request = await fetch(`http://localhost:8000/api/conferences/${id}/`, fetchConfig)
        if (request.ok) {
            const data = await request.json()
            getData()
        }
    }



    console.log("conferences", conferences)
    return(
        <div className="my-5 container">
            <div className="row">
                <h1>Conferences</h1>

                <table className="table table-striped m-3">
                <thead>
                    <tr>
                    <th>Id</th>
                    <th>href</th>
                    <th>Name</th>
                    <th>-</th>
                    <th>-</th>
                    <th>Delete</th>
                    <th>Update</th>
                    </tr>
                </thead>
                <tbody>
                    {conferences.map(conference => {
                    return (
                        <tr key={ conference.id }>
                            <td>{ conference.id }</td>
                            <td>{ conference.href }</td>
                            <td>{ conference.name }</td>
                            <td>{ conference.description }</td>
                            <td>{ conference.location }</td>

                            <td><button onClick={()=>{handleDelete(conference.id)}}>Delete</button></td>
                            <Link className="nav-link" aria-current="page" to="update"><td><button>Update</button></td></Link>
                        </tr>
                    );
                    })}
                </tbody>
                </table>
            </div>
        </div>
    )

    }


    export default ConferenceList
