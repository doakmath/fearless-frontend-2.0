import React, { useEffect, useState } from 'react'

function LocationForm (props) {
    const [states, setStates] = useState([])
    const [name, setName] = useState("")
    const [roomCount, setRoomCount] = useState(0)
    const [cityChange, setCityChange] = useState("")
    const [stateChange, setStateChange] = useState("")

    const handleSubmit = async (event) => {
        event.preventDefault()
        const data = {}
        data.room_count = roomCount
        data.name = name
        data.city = cityChange
        data.state = stateChange

        console.log(data)
        const locationUrl = 'http://localhost:8000/api/locations/'
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };

        const response = await fetch(locationUrl, fetchConfig)
        if (response.ok) {
            const newLocation = await response.json()


            setName('')
            setRoomCount('')
            setCityChange('')
            setStateChange('')


        }
    }


    const handleStateChange = (event) => {
        const value = event.target.value
        setStateChange(value)
    }

    const handleCityChange = (event) => {
        const value = event.target.value
        setCityChange(value)
    }

    const handleRoomCount = (event) => {
        const value = event.target.value
        setRoomCount(value)
    }

    const handleNameChange = (event) => {
        const value = event.target.value;
        setName(value);
    }

    const fetchData = async () => {
        const url = "http://localhost:8000/api/states/"
        const response = await fetch(url);

        if (response.ok) {
          const data = await response.json()
          setStates(data.states)
          }
        }


    useEffect( () => { fetchData() }, [])

    return (
        <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Create a new location</h1>
            <form onSubmit = {handleSubmit} id="create-location-form">
              <div className="form-floating mb-3">
                <input value={name} onChange={handleNameChange} placeholder="Name" required type="text" name="name" id="name" className="form-control" />
                <label htmlFor="name">Name</label>
              </div>
              <div className="form-floating mb-3">
                <input value={roomCount} onChange={handleRoomCount} placeholder="Room count" required type="number" name="room_count" id="room_count" className="form-control" />
                <label htmlFor="room_count">Room count</label>
              </div>
              <div className="form-floating mb-3">
                <input value={cityChange} onChange={handleCityChange} placeholder="City" required type="text" name="city" id="city" className="form-control" />
                <label htmlFor="city">City</label>
              </div>
              <div className="mb-3">
                <select value={stateChange} onChange={handleStateChange} required id="state" name="state" className="form-select">
                  <option value="">Choose a state</option>
                    {states.map(state => {
                        return (
                        <option key={state.abbreviation} value={state.abbreviation}>{state.abbreviation}</option>
                        )
                    })}
                </select>
              </div>
              <button className="btn btn-primary">Create</button>
            </form>
          </div>
        </div>
      </div>
    );
}

export default LocationForm;
